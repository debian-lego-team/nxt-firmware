Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: nxt-firmware
Source: https://nxt-firmware.ni.fr.eu.org/sources/
Upstream-Contact: Nicolas Schodet <nico@ni.fr.eu.org>

Files: *
Copyright: 2006 LEGO
License: LEGO

Files: lib/*
 tests/*
 tools/*
Copyright: 2010 Nicolas Schodet
License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

Files: src/d_sound_adpcm.r
Copyright: 1992 Stichting Mathematisch Centrum
License: permissive-3
 Permission to use, copy, modify, and distribute this software and its
 documentation for any purpose and without fee is hereby granted,
 provided that the above copyright notice appear in all copies and that
 both that copyright notice and this permission notice appear in
 supporting documentation, and that the names of Stichting Mathematisch
 Centrum or CWI not be used in advertising or publicity pertaining to
 distribution of the software without specific, written prior permission.
 .
 STICHTING MATHEMATISCH CENTRUM DISCLAIMS ALL WARRANTIES WITH REGARD TO
 THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 FITNESS, IN NO EVENT SHALL STICHTING MATHEMATISCH CENTRUM BE LIABLE
 FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

Files: armdebug/*
Copyright: 2012 Tat Chee Wan <tcwan@cs.usm.my>
License: LEGO

Files: armdebug/Debugger/abort_handler.S
 armdebug/Debugger/undef_handler.S
Copyright: 2007-2011 TC Wan <tcwan@cs.usm.my>
License: GPL-2.0
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, version 2.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

Files: debian/*
Copyright: 2014, 2016 Dominik George <nik@naturalnet.de>
           2024 Nicolas Schodet <nico@ni.fr.eu.org>
License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, see <https://www.gnu.org/licenses/>.
Comment:
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in "/usr/share/common-licenses/GPL-2".

Files: contrib/nxt-update-firmware
 contrib/nxt-update-firmware.1
 contrib/_incr_version
Copyright: none
License: none
 Does not qualify for copyright protection.

Files: debian/org.eu.fr.ni.nxt_firmware.metainfo.xml
Copyright: 2016 Dominik George <nik@naturalnet.de>
           2021 Petter Reinholdtsen <pere@debian.org>
License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Comment: https://fedoraproject.org/wiki/Licensing/LOSLA
License: LEGO
            LEGO® OPEN SOURCE LICENSE AGREEMENT 1.0
                 LEGO® MINDSTORMS® NXT FIRMWARE
 .
 This LEGO® Open Source License Agreement is an open source license for
 the firmware of the LEGO® MINDSTORMS® NXT microprocessor.
 .
 IMPORTANT -- READ CAREFULLY: THIS AGREEMENT ONLY COVERS THE FIRMWARE OF
 THE LEGO® MINDSTORMS® NXT MICROPROCESSOR AND DOES NOT COVER ANY
 "SOFTWARE" AS DEFINED IN THE MINDSTORMS® NXT END USER LICENSE AGREEMENT
 (HEREINAFTER THE "MINDSTORMS® NXT EULA").
 .
 Section 1. Definitions.
 .
 "Contributor" means each entity that creates or contributes to the
 creation of Modifications.
 .
 "Contributor Version" means the combination of the Original Code, prior
 Modifications used by a Contributor, and the Modifications made by that
 particular Contributor.
 .
 "Covered Code" means the Original Code or Modifications or the
 combination of the Original Code and Modifications, in each case
 including portions thereof.
 .
 "Executable" means Covered Code in any form other than Source Code.
 .
 "Larger Work" means a work which combines Covered Code or portions
 thereof with code not governed by the terms of this License.
 .
 "License" means this document.
 .
 "Modifications" means any addition to or deletion from the substance or
 structure of either the Original Code or any previous Modifications.
 When Covered Code is released as a series of files, a Modification is
 (a) any addition to or deletion from the contents of a file containing
 Original Code or previous Modifications, or (b) any new file that
 contains any part of the Original Code or previous Modifications.
 .
 "Original Code" means Source Code for the firmware of the LEGO®
 MINDSTORMS® NXT microprocessor, and which, at the time of its release
 under this License is not already Covered Code governed by this
 License.
 .
 "Source Code" means the preferred form of the Covered Code for making
 modifications to it, including all modules it contains, plus any
 associated interface definition files, scripts used to control
 compilation and installation of an Executable, or source code
 differential comparisons against either the Original Code or another
 well known, available Covered Code of the Contributor's choice. The
 Source Code can be in a compressed or archival form, provided the
 appropriate decompression or de-archiving software is widely available
 for no charge.
 .
 "You" (or "Your") means an individual or a legal entity exercising
 rights under, and complying with all of the terms of, this License or a
 future version of this License issued under Section 6.1. For legal
 entities, "You'' includes any entity which controls, is controlled by,
 or is under common control with You. For purposes of this definition,
 "control'' means (a) the power, direct or indirect, to cause the
 direction or management of such entity, whether by contract or
 otherwise, or (b) ownership of more than fifty percent (50%) of the
 outstanding shares or beneficial ownership of such entity.
 .
 Section 2. Source Code License and Larger Works.
 .
 2.1.  LEGO Grant.  LEGO grants You a world-wide, royalty-free,
 non-exclusive license, subject to third party intellectual property
 claims, to use, reproduce, modify, display, perform, sublicense and
 distribute the Original Code (or portions thereof) with or without
 Modifications, and/or as part of a Larger Work.
 .
 2.2.  Contributor Grant.   Subject to third party intellectual property
 claims, each Contributor hereby grants You a world-wide, royalty-free,
 non-exclusive license to use, reproduce, modify, display, perform,
 sublicense and distribute the Modifications created by such Contributor
 (or portions thereof) either on an unmodified basis, with other
 Modifications, as Covered Code and/or as part of a Larger Work.
 .
 2.3.  Larger Works.  You may create a Larger Work by combining Covered
 Code with other code not governed by the terms of this License and
 distribute the Larger Work as a single product. In such a case, You
 must make sure the requirements of this License are fulfilled for the
 Covered Code.
 .
 Section 3.  Distribution Obligations.
 .
 3.1.  Application of License. The Modifications which You create or to
 which You contribute shall be governed by the terms of this License,
 including without limitation Section 2.2. The Source Code version of
 Covered Code may be distributed only under the terms of this License or
 a future version of this License released under Section 6.1, and You
 must include a copy of this License with every copy of the Source Code
 You distribute. You may not offer or impose any terms on any Source
 Code version that alters or restricts the applicable version of this
 License or the recipients' rights hereunder.
 .
 3.2.  Availability of Source Code.  Any Modification which You create
 or to which You contribute must be made available in Source Code form
 under the terms of this License either on the same media as an
 Executable version or via a generally accepted mechanism for the
 electronic transfer of data (hereinafter "Electronic Distribution
 Mechanism"), to anyone to whom you made an Executable version
 available; and if made available via Electronic Distribution Mechanism,
 must remain available for at least twelve (12) months after the date it
 initially became available, or at least six (6) months after a
 subsequent version of that particular Modification has been made
 available to such recipients. You are responsible for ensuring that the
 Source Code version remains available even if the Electronic
 Distribution Mechanism is maintained by a third party.
 .
 3.3.  Description of Modifications.  You must cause all Covered Code to
 which You contribute to contain a file documenting the changes You made
 to create that Covered Code and the date of any change. You must
 include a prominent statement that the Modification is derived,
 directly or indirectly, from Original Code provided by LEGO and
 including the name of LEGO® in (a) the Source Code, and (b) in any
 notice in an Executable version or related documentation in which You
 describe the origin or ownership of the Covered Code.
 .
 3.4.  Intellectual Property Matters.
 .
 (a) Third Party Claims. If Contributor has knowledge that a license
 under a third party's intellectual property rights is required to
 exercise the rights granted by such Contributor under Sections 2.1 or
 2.2, Contributor must include a text file with the Source Code
 distribution titled "LEGAL'' which describes the claim and the party
 making the claim in sufficient detail that a recipient will know whom
 to contact. If Contributor obtains such knowledge after the
 Modification is made available as described in Section 3.2, Contributor
 shall promptly modify the LEGAL file in all copies Contributor makes
 available thereafter and shall take other steps (such as notifying
 appropriate mailing lists or newsgroups) reasonably calculated to
 inform those who received the Covered Code that new knowledge has been
 obtained.
 .
 (b)  Contributor APIs. If Contributor's Modifications include an
 application programming interface and Contributor has knowledge of
 patent licenses which are reasonably necessary to implement that API,
 Contributor must also include this information in the LEGAL file.
 .
 (c)  Representations. Contributor represents that, except as disclosed
 pursuant to Section 3.4(a) above, Contributor believes that
 Contributor's Modifications are Contributor's original creation(s)
 and/or Contributor has sufficient rights to grant the rights conveyed
 by this License.
 .
 3.5.  Required Notices. You must duplicate the notice in Exhibit A in
 each file of the Source Code.  If it is not possible to put such notice
 in a particular Source Code file due to its structure, then You must
 include such notice in a location (such as a relevant directory) where
 a user would be likely to look for such a notice.  If You created one
 or more Modification(s) You may add your name as a Contributor to the
 notice described in Exhibit A.  You must also duplicate this License in
 any documentation for the Source Code where You describe recipients'
 rights or ownership rights relating to Covered Code.
 .
 3.6.  Distribution of Executable Versions.  You may distribute Covered
 Code in Executable form only if the requirements of Section 3.1-3.5
 have been met for that Covered Code, and if You include a notice
 stating that the Source Code version of the Covered Code is available
 under the terms of this License, including a description of how and
 where You have fulfilled the obligations of Section 3.2. The notice
 must be conspicuously included in any notice in an Executable version,
 related documentation or collateral in which You describe recipients'
 rights relating to the Covered Code. You must distribute the Executable
 version of Covered Code under the terms of this License.
 .
 Section 4.   Inability to Comply Due to Statute or Regulation.
 .
 If it is impossible for You to comply with any of the terms of this
 License with respect to some or all of the Covered Code due to statute,
 judicial order, or regulation then You must: (a) comply with the terms
 of this License to the maximum extent possible; and (b) describe the
 limitations and the code they affect. Such description must be included
 in the LEGAL file described in Section 3.4 and must be included with
 all distributions of the Source Code. Except to the extent prohibited
 by statute or regulation, such description must be sufficiently
 detailed for a recipient of ordinary skill to be able to understand it.
 .
 Section 5.   Exclusions.
 .
 Notwithstanding anything to the contrary herein, no Software (as
 defined in the Mindstorms® NXT EULA) shall be released or made
 available as open source under this License, regardless of how any
 Covered Code interacts with any Software (as defined in the Mindstorms®
 NXT EULA).
 .
 Section 6.   Versions of the License.
 .
 6.1.  New Versions. LEGO may publish revised and/or new versions of the
 License from time to time. Each version will be given a distinguishing
 version number.
 .
 6.2.  Effect of New Versions. Once Covered Code has been published
 under a particular version of the License, You may always continue to
 use it under the terms of that version. You may also choose to use such
 Covered Code under the terms of any subsequent version of the License
 published by LEGO. No one other than LEGO has the right to modify the
 terms applicable to Covered Code created under this License.
 .
 Section 7.   Disclaimer of Warranty.
 .
 ALL COVERED CODE IS PROVIDED UNDER THIS LICENSE ON AN "AS IS'' BASIS,
 WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 WITHOUT LIMITATION, WARRANTIES THAT THE COVERED CODE IS FREE OF
 DEFECTS, MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE OR NON-INFRINGING.
 THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE COVERED CODE
 IS WITH YOU. SHOULD ANY COVERED CODE PROVE DEFECTIVE IN ANY RESPECT,
 YOU (NOT LEGO OR ANY OTHER CONTRIBUTOR) ASSUME THE COST OF ANY
 NECESSARY SERVICING, REPAIR OR CORRECTION. THIS DISCLAIMER OF WARRANTY
 CONSTITUTES AN ESSENTIAL PART OF THIS LICENSE. NO USE OF ANY COVERED
 CODE IS AUTHORIZED HEREUNDER EXCEPT UNDER THIS DISCLAIMER.
 .
 Section 8. Termination.
 .
 8.1.  This License and the rights granted hereunder will terminate
 automatically if You fail to comply with terms herein and fail to cure
 such breach within 30 days of becoming aware of the breach. All
 sublicenses to the Covered Code which are properly granted shall
 survive any termination of this License. Provisions which, by their
 nature, must remain in effect beyond the termination of this License
 shall survive.
 .
 8.2.  If You initiate litigation by asserting a patent infringement
 claim (excluding declatory judgment actions) against LEGO or a
 Contributor (LEGO or Contributor against whom You file such action is
 referred to as "Participant") alleging that such Participant's
 Contributor Version directly or indirectly infringes any patent, then
 any and all rights granted by such Participant to You under Sections
 2.1 and/or 2.2 of this License shall, upon 60 days notice from
 Participant terminate prospectively, unless if within 60 days after
 receipt of notice You either: (i) agree in writing to pay Participant a
 mutually agreeable reasonable royalty for Your past and future use of
 Modifications made by such Participant, or (ii) withdraw Your
 litigation claim with respect to the Contributor Version against such
 Participant.  If within 60 days of notice, a reasonable royalty and
 payment arrangement are not mutually agreed upon in writing by the
 parties or the litigation claim is not withdrawn, the rights granted by
 Participant to You under Sections 2.1 and/or 2.2 automatically
 terminate at the expiration of the 60 day notice period specified
 above.
 .
 8.3.  If You assert a patent infringement claim against Participant
 alleging that such Participant's Contributor Version directly or
 indirectly infringes any patent where such claim is resolved (such as
 by license or settlement) prior to the initiation of patent
 infringement litigation, then the reasonable value of the licenses
 granted by such Participant under Sections 2.1 or 2.2 shall be taken
 into account in determining the amount or value of any payment or
 license.
 .
 8.4.  In the event of termination under Sections 8.1 or 8.2 above,  all
 end user license agreements (excluding distributors and resellers)
 which have been validly granted by You or any distributor hereunder
 prior to termination shall survive termination.
 .
 Section 9.  Limitation of Liability.
 .
 UNDER NO CIRCUMSTANCES AND UNDER NO LEGAL THEORY, WHETHER TORT
 (INCLUDING NEGLIGENCE), CONTRACT, OR OTHERWISE, SHALL YOU, LEGO, ANY
 OTHER CONTRIBUTOR, OR ANY DISTRIBUTOR OF COVERED CODE, OR ANY SUPPLIER
 OF ANY OF SUCH PARTIES, BE LIABLE TO ANY PERSON FOR ANY INDIRECT,
 SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES OF ANY CHARACTER
 INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF GOODWILL, WORK
 STOPPAGE, COMPUTER FAILURE OR MALFUNCTION, OR ANY AND ALL OTHER
 COMMERCIAL DAMAGES OR LOSSES, EVEN IF SUCH PARTY SHALL HAVE BEEN
 INFORMED OF THE POSSIBILITY OF SUCH DAMAGES. THIS LIMITATION OF
 LIABILITY SHALL NOT APPLY TO LIABILITY FOR DEATH OR PERSONAL INJURY
 RESULTING FROM SUCH PARTY'S NEGLIGENCE TO THE EXTENT APPLICABLE LAW
 PROHIBITS SUCH LIMITATION. SOME JURISDICTIONS DO NOT ALLOW THE
 EXCLUSION OR LIMITATION OF INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THIS
 EXCLUSION AND LIMITATION MAY NOT APPLY TO YOU.
 .
 Section 10.  U.S. Government End Users.
 .
 The Covered Code is a "commercial item," as that term is defined in
 48 C.F.R. 2.101 (Oct. 1995), consisting of "commercial computer
 software" and "commercial computer software documentation," as such
 terms are used in 48 C.F.R. 12.212 (Sept. 1995). Consistent with 48
 C.F.R. 12.212 and 48 C.F.R. 227.7202-1 through 227.7202-4 (June 1995),
 all U.S. Government End Users acquire Covered Code with only those
 rights set forth herein.
 .
 Section 11.  Responsibility for Claims.
 .
 As between LEGO and the Contributors, each party is responsible for
 claims and damages arising, directly or indirectly, out of its
 utilization of rights under this License and You agree to work with
 LEGO and Contributors to distribute such responsibility on an equitable
 basis. Nothing herein is intended or shall be deemed to constitute any
 admission of liability.
 .
 Section 12.   Trademarks.
 .
 This License does not grant, nor shall any provision of this License be
 construed as granting, any rights or permission to use the trade names,
 trademarks, service marks, or product names of LEGO.
 .
 Section 13.  Governing Law
 .
 To the extent possible under applicable law, this License shall be
 governed by Danish law and shall be subject to the non-exclusive
 jurisdiction of the Commercial and Maritime Court of Copenhagen. This
 License will not be governed by the United Nations Convention of
 Contracts for the International Sale of Goods, the application of which
 is hereby expressly excluded. You acknowledge that the export of any
 Covered Code is governed by the export control laws of the United
 States of America and other countries. If you are downloading any
 Covered Code, You represent and warrant that You are not located in or
 under the control of any country which the export laws and regulations
 of such country or of the United States prohibit the exportation of the
 Covered Code.
 .
 Section 14.  Miscellaneous.
 .
 This License represents the complete agreement concerning subject
 matter hereof. If any provision of this License is held to be
 unenforceable, such provision shall be reformed only to the extent
 necessary to make it enforceable. Any law or regulation which provides
 that the language of a contract shall be construed against the drafter
 shall not apply to this License.
 .
 EXHIBIT A - LEGO® Open Source License Agreement
 .
 The contents of this file are subject to the LEGO® Open Source License
 Agreement Version 1.0 (the "License"); you may not use this file except
 in compliance with the License. You may obtain a copy of the License at
 http://www.______________.com
 .
 Software distributed under the License is distributed on an "AS IS"
 basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 License for the specific language governing rights and limitations
 under the License.
 .
 The Original Code is ______________________________________.
 .
 LEGO is the owner of the Original Code.  Portions created by
 _________________ are Copyright (C) _________. All Rights Reserved.
 .
 Contributor(s): ______________________________________.
